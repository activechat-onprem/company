### Quick Setup

#### 1. Create a Virtual Machine (VM)
- **Cloud Provider**: While any cloud provider can be used, we recommend Digital Ocean (UPD: after the initial version of these instructions has been released, we've been approached by and subsequently partenered with Digital Ocean; to get a $200 headstart, use this referral link: https://m.do.co/c/c8f45d281273)
- **Docker**: Ensure Docker (version >= 22) is installed on your VM. On Digital Ocean, you'll need to scroll down to Choose your image section, open the Marketplace tab and select Docker on Ubuntu.
- **Docker permissions**: If you are not going to work as root, which is a good idea, please, do the following:
```bash
sudo usermod -aG docker $USER
newgrp docker
```
- **Specifications**: A VM with 2 CPUs and 4 GB of RAM is a minimum requirement.
- **Ports**: Ports 80 and 443 should be open on your VM firewall.


#### 2. Docker login
```bash
docker login registry.activechat.ai -u <username> -p <your-password> 
```

#### 3. Clone the project to your VM
```bash
git clone https://gitlab.com/activechat-onprem/company.git
cd company
```

#### 4. Edit `config.env` to update your DNS records and logo
```bash
cp config.env.orig config.env
nano config.env
``` 
Edit the data accroding your domain and mongo settings.  
To ensure functionality, we require 7 domain names that point to your VM's IP address:
- CONSTRUCTOR_DOMAIN
- ACCESS_DOMAIN
- LIVECHAT_DOMAIN
- CHATWIDGET_DOMAIN
- ARCHITECTOR_DOMAIN
- CIS_DOMAIN
- USER_DOMAIN

Create 7 separate DNS records.  
Type: A  
Names: access, app, architector-demo, chatwidget, cis-demo, livechat, user  
Value: ipv4 from Digital Ocean  

Add links to your Privacy Policy and Terms of Service. If you choose not to use them, delete the default <your-link-here> and leave the space after the = sign empty.   
The logo can be uploaded through any open web hosting with a direct file link.  
Ctrl+O, Enter, CTRL+X to save and exit.

#### 5. Start your application
Execute the following command to start your application:
```bash
./start.sh
```
Please note that the very first start will take up to 15 minutes.

After the start is finished, close the console, open <CONSTRUCTOR_DOMAIN> in your browser and give the platform a go. You're all set to run your Saas now, congratulations! :)
